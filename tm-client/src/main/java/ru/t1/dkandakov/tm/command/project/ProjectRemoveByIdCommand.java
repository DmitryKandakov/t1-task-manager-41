package ru.t1.dkandakov.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.dkandakov.tm.dto.request.project.ProjectRemoveByIdRequest;
import ru.t1.dkandakov.tm.dto.response.project.ProjectRemoveByIdResponse;
import ru.t1.dkandakov.tm.exception.entity.ProjectNotFoundException;
import ru.t1.dkandakov.tm.model.Project;
import ru.t1.dkandakov.tm.util.TerminalUtil;

public final class ProjectRemoveByIdCommand extends AbstractProjectCommand {

    @Override
    public void execute() {
        System.out.println("[REMOVE PROJECT BY ID]");
        System.out.println("ENTER ID: ");
        @Nullable final String id = TerminalUtil.nextLine();
        @NotNull ProjectRemoveByIdRequest request = new ProjectRemoveByIdRequest(getToken(), id);
        @NotNull ProjectRemoveByIdResponse response = getProjectEndpoint().removeProjectById(request);
        @Nullable final Project project = response.getProject();
        if (project == null) throw new ProjectNotFoundException();
    }

    @NotNull
    @Override
    public String getName() {
        return "project-remove-by-id";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Remove project by id.";
    }

}