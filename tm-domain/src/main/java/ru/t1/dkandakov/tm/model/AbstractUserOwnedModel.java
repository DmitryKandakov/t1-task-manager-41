package ru.t1.dkandakov.tm.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;

@Setter
@Getter
@NoArgsConstructor
@MappedSuperclass
public class AbstractUserOwnedModel extends AbstractModel {

    @Nullable
    @Column(name = "user_id")
    private String userId;

    public AbstractUserOwnedModel(@Nullable String userId) {
        this.userId = userId;
    }

}
